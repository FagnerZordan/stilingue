"use strict";

angular
  .module("myApp.home", ["ngRoute"])

  .config([
    "$routeProvider",
    function($routeProvider) {
      $routeProvider.when("/home", {
        templateUrl: "home/home.html",
        controller: "homeCtrl"
      });
    }
  ])

  .controller("homeCtrl", function($scope, $http) {
    $scope.createObjectScope = createObjectScope;
    $scope.createGraph = createGraph;
    $scope.setLayout = setLayout;
    $scope.setShape = setShape;
    $scope.setBackgroundColor = setBackgroundColor;
    $scope.setLineColor = setLineColor;
    $scope.setResultType = setResultType;

    var myData;

    $scope.resultTypeList = [
      { id: 0, name: "relations" },
      {
        id: 1,
        name: "synonymous"
      }
    ];

    $scope.layoutList = [
      "circle",
      "grid",
      "random",
      "concentric",
      "breadthfirst"
    ];

    $scope.shapeList = [
      "ellipse",
      "triangle",
      "barrel",
      "rhomboid",
      "diamond",
      "pentagon",
      "octagon",
      "star",
      "vee"
    ];

    $scope.backgroundColorList = [
      "blue",
      "red",
      "yellow",
      "purple",
      "pink",
      "grey",
      "green"
    ];

    $scope.lineColorList = [
      "blue",
      "red",
      "yellow",
      "purple",
      "pink",
      "grey",
      "green"
    ];

    $scope.layout = $scope.layoutList[0];
    $scope.shape = $scope.shapeList[0];
    $scope.backgroundColor = $scope.backgroundColorList[0];
    $scope.lineColor = $scope.lineColorList[0];
    $scope.resultType = "1";

    $scope.nameList = [];
    $scope.name;
    $scope.meanings;
    $scope.otherMeanings;
    $scope.relations;
    $scope.synonymous;
    $scope.sequence;

    function _getNameList() {
      for (var i = 0; i < myData.length; i++) {
        var nameObject = {
          text: myData[i].result.extractorData.data[0].group[0].name[0].text,
          sequence: myData[i].result.sequenceNumber
        };
        $scope.nameList.push(nameObject);
      }
    }

    function createObjectScope(sequenceFilter) {
      $scope.name =
        myData[
          sequenceFilter
        ].result.extractorData.data[0].group[0].name[0].text;
      $scope.pronounce =
        myData[
          sequenceFilter
        ].result.extractorData.data[0].group[0].pronounce[0].text;
      $scope.meanings =
        myData[sequenceFilter].result.extractorData.data[0].group[0].meanings;
      $scope.quotes =
        myData[sequenceFilter].result.extractorData.data[0].group[0].quotes;
      $scope.relations =
        myData[sequenceFilter].result.extractorData.data[0].group[0].relations;
      $scope.synonymous =
        myData[sequenceFilter].result.extractorData.data[0].group[0].synonymous;
      $scope.sequence = sequenceFilter;

      createGraph();
    }

    function createGraph() {
      $scope.cy = cytoscape({
        container: document.getElementById("cy"),
        elements: [],
        style: [
          {
            selector: "node",
            style: {
              shape: $scope.shape,
              "background-color": $scope.backgroundColor,
              label: "data(id)"
            }
          },
          {
            selector: "edge",
            style: {
              "line-color": $scope.lineColor
            }
          }
        ]
      });

      var name = $scope.name;

      $scope.cy.add({
        data: { id: name }
      });
      var sourceName = name;

      if ($scope.resultType === "0") {
        _synonymouGrap($scope.synonymous, sourceName);
      } else if ($scope.resultType === "1") {
        _relationGraph($scope.relations, sourceName);
      }

      $scope.cy.layout({
        name: $scope.layout
      });
    }

    function _synonymouGrap(synonymous, sourceName) {
      if (synonymous != null && synonymous.length) {
        for (var j = 0; j < synonymous.length; j++) {
          var synonymousName = synonymous[j].text;

          $scope.cy.add({
            data: { id: synonymousName }
          });
          var sourceSynonymousName = synonymousName;

          $scope.cy.add({
            data: {
              id: sourceName + sourceSynonymousName,
              source: sourceName,
              target: sourceSynonymousName
            }
          });
        }
      }
    }

    function _relationGraph(relations, sourceName) {
      if (relations != null && relations.length) {
        for (var j = 0; j < relations.length; j++) {
          var relationsName = relations[j].text;

          $scope.cy.add({
            data: { id: relationsName }
          });
          var sourceRelationsName = relationsName;

          $scope.cy.add({
            data: {
              id: sourceName + sourceRelationsName,
              source: sourceName,
              target: sourceRelationsName
            }
          });
        }
      }
    }

    $http.get("home/data.json").then(function(response) {
      myData = response.data;
      _getNameList();
      createObjectScope(0);
    });

    function setLayout(layout) {
      $scope.layout = layout;
      createGraph();
    }

    function setShape(shape) {
      $scope.shape = shape;
      createGraph();
    }

    function setBackgroundColor(backgroundColor) {
      $scope.backgroundColor = backgroundColor;
      createGraph();
    }

    function setLineColor(lineColor) {
      $scope.lineColor = lineColor;
      createGraph();
    }

    function setResultType(resultType) {
      $scope.resultType = resultType;
      createGraph();
    }
  })
  .run();
